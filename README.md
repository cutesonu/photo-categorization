# Photo-Categorization

### Categories
- Building
- Drink:      Beer / Wine / Cocktail / etc.
- Food:       Type of dish (eg: hot dog / fried chicken / etc.), Products visible (eg: avocado / etc.)
- Interior:   Restaurant / Bar / etc.
- Object
- Other


### Steps

- Step1: Clone the project and Install Dependencies

            
        git clone https://gitlab.com/cutesonu/photo-categorization
        
        pip3 install -r requirements.txt
        
        cd photo-categorization
        

- Step2: Prepare the Training Data

Prepare the images which is classified already depending its label information. And move the classified images to the `data/train`.
At this moment, the sub-folder name on `data/train` should be same matched with the label name.
        
        root(photo-categorization)
        |
        ├── data                        
        │   ├── train                     # train data
        │   │   ├── Building
        │   │   ├── Drink
        │   │   ├── Food
        │   │   ├── Interior
        │   │   ├── Object
        │   │   └── Other
        ...
        
        

- Step3: Extract Embedding feature

Extract the embedding features through train data and save them as csv file format. The each feature vector is float vector (length 1024).
The pre-trained Inception CNN model was used to extract the embedding feature. 
As a result, bellow 2 files will be created. `data/features/train_data.csv` and `data/features/train_label.txt`
    
    `$ python3 endpoint.py --mode feature`
     
        ```
        root(photo-categorization)     
        ├── data
        |   ├── train 
        |   └── features                # embedded features
        |       ├── train_data.csv
        |       └── train_label.txt
        ...
        ```

- Step4: Train/Validate the trained classifier model

The classifier model (SVM) model based on sklearn will be trained with embedded features data prepared on above step.
It is able to check the trained model with validate image files, change the `mode` param from `train` to `validate`. As a result the confusion matrix will come out.
The trained classifier model will be saved on `models/classifer/classifier.pkl`.  
    
    `$ python3 endpoint.py --mode train`
    `$ python3 endpoint.py --mode validate` 
       
        ```    
        root(photo-categorization)
        ├── data                          # data
        |
        ├── models                         # models
        |    ├── classifier                  # SVM classifier model
        |    └── imgnet                      # inception model, this be downloaded automatically
        |
        ├── src                            # src
        |    ├── classify.py                 # utilties for classifier model 
        |    ├── down_load.py                # download the pre-trained model from the cloud storage 
        |    ├── features.py                 # extract embedding features from the image file or cv image 
        |    ├── imgnet.py                   # inception CNN utils
        |    ├── node_lookup.py              # inception CNN utils based on human string lookup labels
        │    ├── pre-proc.py                 # utilities to preprocess the train data before feature extraction
        |    ├── train.py                    # train
        │    └── train_data                  # manage the train data
        |
        ├── utils                       # common utils
        │   ├── common.py                    # utilities to preprocess the train data before feature extraction
        |    ├── constant.py                 # definition of contants e.g. path of models
        │   └── settings.py                  # manage the train data
        |
        └── ...
        ```
    

- Step5: Test

Test with new image, which will return the predicted(top candidate) label name and its confidence.
   
    `$ python3 endpoint.py --mode test ` 

        ```
        root(photo-categorization)
        ├── data                            # data
        |
        ├── models                          @ models
        |
        ├── src                             @ src
        |
        ├── utils                           @ utils
        |
        └── endpoint.py
                
        ```
