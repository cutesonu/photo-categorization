import os
import cv2


from src.imgnet import ImgNet
from src.node_lookup import NodeLookup
from utils.constant import DATA_DIR, LABELS, LABEL_KEYWORDS


inu = ImgNet(mode="lookup")
node_lookup = NodeLookup()


def predict_lookup_labels(predictions):
    num_top_candis = 5
    top_k = predictions.argsort()[-num_top_candis:][::-1]

    str_lookup_labels = []
    for node_id in top_k:
        human_string = node_lookup.id_to_string(node_id)
        str_lookup_labels.append(human_string)

    return str_lookup_labels


def lookup_categorize(raw_dir):
    paths = [os.path.join(raw_dir, fn) for fn in os.listdir(raw_dir)]
    paths.sort()
    paths = sorted(paths, key=lambda i: int(os.path.splitext(os.path.basename(i))[0]))

    counts = [0] * len(LABELS)
    for path in paths:
        feature = inu.get_feature_from_image(img_path=path)

        lookup_labels = predict_lookup_labels(feature)

        img = cv2.imread(path)
        cv2.imshow("image", cv2.resize(img, (500, 500)))
        print(lookup_labels)
        cv2.waitKey(0)

        cat_idx = -1
        for lookup_label in lookup_labels:
            for label_id, label in enumerate(LABELS):
                keywords = LABEL_KEYWORDS[label_id]
                if len(keywords) == 0:
                    continue
                for keyword in LABEL_KEYWORDS[label_id]:
                    if lookup_label.lower().find(keyword.lower()) != -1:
                        cat_idx = label_id
                        break
            if cat_idx != -1:
                break

        label_id = cat_idx

        new_path = os.path.join(DATA_DIR, LABELS[label_id], os.path.split(path)[1])
        os.rename(path, new_path)

        counts[label_id] += 1
        print(path, "->", LABELS[label_id])
    print(counts)


def unique_indexing(src_dir, dst_dir):
    paths = [os.path.join(src_dir, fn) for fn in os.listdir(src_dir)]
    paths.sort()

    for idx, path in enumerate(paths):
        img = cv2.imread(path)
        try:
            if img is not None:
                new_path = os.path.join(dst_dir, "{}.jpg".format(idx))
                cv2.imwrite(new_path, img)
        except Exception as e:
            print(e, path)


if __name__ == '__main__':
    # unique_indexing(src_dir=os.path.join(DATA_DIR, os.pardir, os.pardir, 'photos'),
    #                 dst_dir=os.path.join(DATA_DIR, os.pardir, os.pardir, 'raw_data'))
    lookup_categorize(raw_dir=os.path.join(DATA_DIR, os.pardir, os.pardir, 'raw_data'))
