import os
import sys
import tarfile
from six.moves import urllib


def download_and_extract_model(data_url, save_dir):
    # Download and extract imgnet tar file.

    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    file_name = data_url.split('/')[-1]
    file_path = os.path.join(save_dir, file_name)
    if not os.path.exists(file_path):
        def _progress(count, block_size, total_size):
            sys.stdout.write("\r>> Downloading %s %.1f%%" % (
                file_name, float(count * block_size) / float(total_size) * 100.0))
            sys.stdout.flush()

        file_path, _ = urllib.request.urlretrieve(data_url, file_path, _progress)
        stat_info = os.stat(file_path)
        print("\nSuccessfully downloaded {} {} bytes.\n".format(file_name, stat_info.st_size))
    tarfile.open(file_path, 'r:gz').extractall(save_dir)
