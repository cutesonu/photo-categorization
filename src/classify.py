import cv2
import sys
import os
import numpy as np
from sklearn.externals import joblib


from src.imgnet import ImgNet
from utils.constant import CLASSIFIER_DIR


class Classifier:
    def __init__(self):
        self.emb = ImgNet()
        self.classifier = self.load_classifier_model()

    @staticmethod
    def load_classifier_model():
        classifier_path = os.path.join(CLASSIFIER_DIR, "classifier.pkl")

        if not os.path.exists(classifier_path):
            print(" not exist trained classifier {}\n".format(classifier_path))
            sys.exit(1)

        try:
            # loading
            model = joblib.load(classifier_path)
            return model
        except Exception as ex:
            print(ex)
            sys.exit(1)

    def predict(self, img_path="", img=None):
        if img_path != "":
            cvimg = cv2.imread(img_path)
        elif img is not None:
            cvimg = img.copy()
        else:
            return "not indicated source image"

        feature = self.emb.get_feature_from_cv_mat(cvimg=cvimg)
        feature = feature.reshape(1, -1)

        # Get a prediction from the imgnet including probability:
        probab = self.classifier.predict_proba(feature)

        max_ind = np.argmax(probab)
        sort_probab = np.sort(probab, axis=None)[::-1]  # Rearrange by size

        if sort_probab[0] / sort_probab[1] < 0.7:
            predlbl = "UnKnown"
        else:
            predlbl = self.classifier.classes_[max_ind]

        return predlbl
