import sys
import os
import csv
import numpy as np
from sklearn.svm import SVC
from sklearn.externals import joblib


from src.imgnet import ImgNet
from utils.constant import FEATURES_DIR, CLASSIFIER_DIR


def load_feature_and_label():
    print('load feature and labels')

    feature_data_path = os.path.join(FEATURES_DIR, "train_data.csv")
    feature_label_path = os.path.join(FEATURES_DIR, "train_label.txt")

    if not os.path.exists(feature_data_path):
        print(" not exist train data {}".format(feature_data_path))
        sys.exit(0)
    if not os.path.exists(feature_label_path):
        print(" not exist train label {}".format(feature_label_path))
        sys.exit(0)

    data = []
    labels = []
    label_names = []
    # --- loading labels ---------------------------------------------------------------------
    print(' loading training labels ... ')
    with open(feature_label_path, 'r') as fp:
        for line in fp:
            line = line.replace('\n', '')
            label_names.append(line)

    # --- loading data -----------------------------------------------------------------------
    print(' loading training features ... ')
    with open(feature_data_path) as fp:
        csv_reader = csv.reader(fp, delimiter=',')
        for row in csv_reader:
            _feature = [float(row[i]) for i in range(0, len(row))]
            _label = _feature[-len(label_names):]
            data.append(np.asarray(_feature[:-len(label_names)]))

            label_idx = -1
            for i in range(len(label_names)):
                if _label[i] == 1.0:
                    label_idx = i
                    break
            if label_idx != -1:
                labels.append(label_names[label_idx])
            else:
                print(' error on tails for label indicator')
                sys.exit(0)
    return data, labels, label_names


def train():
    print('>>> train')
    classifier_path = os.path.join(CLASSIFIER_DIR, "classifier.pkl")
    if not os.path.exists(CLASSIFIER_DIR):
        os.mkdir(CLASSIFIER_DIR)

    # --- load feature and label data ----------------------------------------------------------------
    data, labels, label_names = load_feature_and_label()

    # --- training -----------------------------------------------------------------------------------
    print(' training... ')
    classifier = SVC(C=1.0, kernel='linear', degree=3, gamma='auto', coef0=0.0, shrinking=True, probability=True,
                     tol=0.001, cache_size=200, class_weight='balanced', verbose=False, max_iter=-1,
                     decision_function_shape='ovr', random_state=None)

    classifier.fit(data, labels)
    joblib.dump(classifier, classifier_path)

    print('>>> finished the training!')


def load_classifier_model():
    classifier_path = os.path.join(CLASSIFIER_DIR, "classifier.pkl")

    if not os.path.exists(classifier_path):
        print(" not exist trained classifier {}".format(classifier_path))
        sys.exit(0)

    try:
        # loading
        model = joblib.load(classifier_path)
        return model
    except Exception as ex:
        print(ex)
        sys.exit(0)


def predict(classifier, feature):
    feature = feature.reshape(1, -1)

    # Get a prediction from the imgnet including probability:
    probab = classifier.predict_proba(feature)

    max_ind = np.argmax(probab)
    sort_probab = np.sort(probab, axis=None)[::-1]  # Rearrange by size

    if sort_probab[0] / sort_probab[1] < 0.7:
        predlbl = "UnKnown"
    else:
        predlbl = classifier.classes_[max_ind]

    return predlbl, sort_probab[0]


def check_precision():
    # --- load trained classifier imgnet --------------------------------------------------------------
    print('>>> checking the precision...')
    classifier = load_classifier_model()

    # --- load feature and label data ----------------------------------------------------------------
    data, labels, label_names = load_feature_and_label()

    confusion_matrix = np.zeros((len(label_names), len(label_names)), dtype=np.int)

    positive, negative = 0, 0
    # --- check confuse matrix ------------------------------------------------------------------------
    for i in range(len(data)):
        feature = data[i]

        predlbl, score = predict(classifier=classifier, feature=feature)

        # update confusion matrix
        predlbl_idx = label_names.index(predlbl)
        ground_truth_idx = label_names.index(labels[i])

        confusion_matrix[predlbl_idx][ground_truth_idx] += 1
        if predlbl_idx == ground_truth_idx:
            positive += 1
        else:
            negative += 1

    print('>>> precision result')
    print(confusion_matrix)

    precision = positive / (positive + negative)
    print("\tprecision : {} / {} : {}%".format(positive, positive + negative, round(precision * 100, 2)))


def test(image_path):
    # Extract the feature vector per each image
    inu = ImgNet()
    classifier = load_classifier_model()

    feature = inu.get_feature_from_image(img_path=image_path)
    feature.tolist()

    pred_lbl, pred_score = predict(classifier=classifier, feature=feature)

    print("predict result: ")
    print("\timage path: {}".format(image_path))
    print("\tlabel: {}, confidence: {}".format(pred_lbl, round(pred_score, 2)))
    return pred_lbl, pred_score


if __name__ == '__main__':
    train()
    check_precision()
