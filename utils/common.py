import os

from utils.constant import IMG_EXTs


def allowed_file(filename):
    return '.' in filename and os.path.splitext(filename)[1].lower() in IMG_EXTs
