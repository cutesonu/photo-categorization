import os

# --------------------------------------- [PATHS] ----------------------------------------------------------------------
_cur_dir = os.path.dirname(os.path.realpath(__file__))

# ROOT
ROOT_DIR = os.path.join(_cur_dir, os.pardir)

SRC_DIR = os.path.join(ROOT_DIR, 'src')

DATA_DIR = os.path.join(ROOT_DIR, 'data')
TRAIN_DATA_DIR = os.path.join(DATA_DIR, "train")
FEATURES_DIR = os.path.join(DATA_DIR, "features")

MODEL_DIR = os.path.join(ROOT_DIR, 'models')
CLASSIFIER_DIR = os.path.join(MODEL_DIR, "classifier")
IMGNET_DIR = os.path.join(MODEL_DIR, "imgnet")


# --------------------------------------- [INPUT DATA] -----------------------------------------------------------------
IMG_EXTs = ['.png', '.jpeg', '.jpg', '.bmp']  # allowed image extensions


FEATURE_DESC_MODE = "imgnet"


LABELS = ["Building", "Drink",  "Food",  "Interior", "Object", "Other"]

LABEL_KEYWORDS = [
    ["interior", "Restaurant", "Bar", 'shop'],
    ["drink", "Beer", "Wine", "Cocktail", 'cup', 'candle', 'taper',
     'icecream', 'eggnog', 'bottle'],
    ["food", "hot dog", "hotdog", "fried chicken", 'cheeseburger', 'hot pot', 'hotpot',
     'wok', 'frying', 'French horn', 'dish', 'meat',
     'bakery', 'fig ', 'slug'],
    ["Building"],
    [],
    []
]
