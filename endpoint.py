import os
import sys
import argparse


from src.features import collect_features
from src.train import train, check_precision, test


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="endpoint params")

    parser.add_argument('-m', '--mode', help='mode for running the endpoint', required=True, default='test')
    parser.add_argument('-i', '--image', help='image path to test the model', required=False, default='test.jpg')

    args = parser.parse_args()

    mode = args.mode
    image_path = args.image
    if mode == 'feature':
        collect_features()
    elif mode == 'train':
        train()
    elif mode == 'validate':
        check_precision()
    elif mode == 'test':
        if not image_path:
            sys.stderr.write("invalidate the input path image\n")
            sys.exit(1)
        elif not os.path.exists(image_path):
            sys.stderr.write("no exist such image file {}\n".format(image_path))
            sys.exit(1)
        else:
            test(image_path=image_path)
